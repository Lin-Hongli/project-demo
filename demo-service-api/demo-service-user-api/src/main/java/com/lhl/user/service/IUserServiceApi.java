package com.lhl.user.service;

import com.lhl.user.model.dto.UserDTO;
import com.lhl.user.service.hystrix.UserServiceHystrix;
import com.lhl.util.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 开放接口层：
 * 可直接封装 Service 方法暴露成 RPC 接口；
 * 通过 Web 封装成 http 接口；
 * 进行网关安全控制、流量控制等。
 *
 * @author LinHongli
 */
@FeignClient(value = "demo-service-user",fallback = UserServiceHystrix.class)
public interface IUserServiceApi {
    String API_PREFIX = "/api/user";

    /**
     * 获取用户信息
     * @param id 唯一标识
     * @return Result<Object>
     */
    @GetMapping(API_PREFIX+"/getUser")
    Result<Object> getUser(@RequestParam("id") Long id);

    /**
     * 保存
     * @param userDTO User传输对象
     * @return Result<Object>
     */
    @GetMapping(API_PREFIX+"/save")
    Result<Object> save(UserDTO userDTO);
}
