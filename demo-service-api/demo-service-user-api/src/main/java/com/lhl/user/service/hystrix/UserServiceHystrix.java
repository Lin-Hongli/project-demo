package com.lhl.user.service.hystrix;

import com.lhl.base.emuns.ErrorCodeEnum;
import com.lhl.user.model.dto.UserDTO;
import com.lhl.user.service.IUserServiceApi;
import com.lhl.util.result.Result;

/**
 * 服务触发熔断时的返回
 *
 * @author LinHongli
 */
public class UserServiceHystrix implements IUserServiceApi {


    @Override
    public Result<Object> getUser(Long id) {
        return Result.fail(ErrorCodeEnum.B0200);
    }

    @Override
    public Result<Object> save(UserDTO userDTO) {
        return Result.fail(ErrorCodeEnum.B0200);
    }
}
