package com.lhl.user.model.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author LinHongli
 */
@Setter
@Getter
@ToString
public class UserDTO {
}
