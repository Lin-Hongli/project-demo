package com.lhl.user.web.rpc;

import com.lhl.user.model.dto.UserDTO;
import com.lhl.user.service.IUserServiceApi;
import com.lhl.util.result.Result;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 这里一般只负责调用对应的service
 * 多个service操作时，才做调用的逻辑处理
 *
 * @author LinHongli
 */
@RefreshScope
public class UserServiceClient implements IUserServiceApi {

    @Override
    public Result<Object> getUser(Long id) {
        return null;
    }

    @Override
    public Result<Object> save(UserDTO userDTO) {
        return null;
    }
}
