package com.lhl.user.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhl.base.emuns.BusinessErrorCodeEnum;
import com.lhl.base.exception.BusinessException;
import com.lhl.user.model.entity.User;
import com.lhl.user.model.query.UserQuery;
import com.lhl.user.model.vo.UserVO;
import com.lhl.user.service.IUserService;
import com.lhl.util.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Web 层：主要是对访问控制进行转发，各类基本参数校验，或者不复用的业务简单处理等。
 * #统一返回Result对象
 * 所有函数返回统一的Result/PageResult格式，没有统一格式，AOP无法玩，更加重要的是前台代码很不好写。
 * #Result不允许往后传
 * Result/PageResult是controller专用的，不允许往后传！往其他地方传之后，可读性立马下降，和传map，json好不了多少。
 * #Controller只做参数格式的转换
 * Controller做参数格式的转换，不允许把json，map这类对象传到services去，也不允许services返回json、map。
 * 写过代码都知道，map，json这种格式灵活，但是可读性差（ 编码一时爽，重构火葬场）。
 * 如果放业务数据，每次阅读起来都十分困难，需要从头到尾看完才知道里面有什么，是什么格式。
 * 定义一个bean看着工作量多了，但代码清晰多了。
 * #参数不允许出现Request，Response 这些对象,和json/map一样，主要是可读性差的问题。一般情况下不允许出现这些参数，除非要操作流。
 * #不需要打印日志,日志在AOP里面会打印，建议是大部分日志在Services这层打印。
 * ###建议
 * Contorller只做参数格式转换，如果没有参数需要转换的，那么就一行代码。日志/参数校验/权限判断建议放到service里面，毕竟controller基本无法重用，而service重用较多。而我们的单元测试也不需要测试controller，直接测试service即可。
 *
 * @author LinHongli
 */
@RestController
@RequestMapping("/user")
@Api("非restful风格的controller")
public class UserController {
    @Resource
    IUserService userService;


    /**
     * 新增
     *
     * 有通用异常处理，参数列表不需要跟BindingResult bindingResult
     */
    @PostMapping(value = "/save")
    @ApiOperation(value = "新增", notes = "传入userQuery")
    public Result<Object> save(@Validated({UserQuery.Save.class}) @RequestBody UserQuery userQuery) {
        User user=new User();
        BeanUtils.copyProperties(userQuery,user);
        return Result.success(userService.save(user));
    }

    /**
     * 删除
     */
    @PostMapping(value = "/remove")
    @ApiOperation(value = "删除", notes = "传入userQuery")
    public Result<Object> remove(@RequestParam(name = "id") @NotEmpty(message = "id不能为空") String id) {
//        Func.toLongList(ids)
        return Result.success(userService.removeById(Long.valueOf(id)));
    }

    /**
     * 修改
     */
    @PostMapping("/modify")
    @ApiOperation(value = "修改", notes = "传入userQuery")
    public Result<Object> modify(@Validated(UserQuery.Update.class) @RequestBody UserQuery userQuery) {
        User user=new User();
        BeanUtils.copyProperties(userQuery,user);
        user.setId(Long.valueOf(userQuery.getId()));

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getId,user.getId());
        boolean update = userService.update(user, queryWrapper);
        if(!update){
            throw new BusinessException(BusinessErrorCodeEnum.USER10010003,user.getId());
        }
        return Result.success();
    }

    /**
     * 详情
     *
     * URL 路径不能使用大写 单词如果需要分隔，统一使用下划线。
     */
    @ApiOperation(value = "根据id查询(接口说明)", httpMethod = "GET(接口请求方式)", response = Result.class, notes = "1.0.0(接口发布说明)")
    @ApiParam(required = true, name = "id(参数名称)", value = "这是一个id(参数具体描述)")
    @GetMapping(path = "/query")
    public Result<Object> query(@RequestParam(name = "id") @NotEmpty(message = "id不能为空") String id) {
        User user = userService.getById(Long.valueOf(id));
        UserVO userVO=new UserVO();
        BeanUtils.copyProperties(user,userVO);
        return Result.success(userVO);
    }

    /**
     * 列表
     */
    @ApiOperation(value = "根据id集合查询")
    @GetMapping("/list")
    public Result<Object> list() {
        List<User> userList = userService.list();
        List<UserVO> userVOList = new ArrayList<>();
        userList.forEach(item->{
            UserVO userVO=new UserVO();
            BeanUtils.copyProperties(item,userVO);
            userVOList.add(userVO);

        });
        return Result.success(userVOList);
//                return R.data(NoticeWrapper.build().pageVO(pages));
    }

    /**
     * 分页
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Object> page(@RequestParam(defaultValue = "1") Integer current,
                       @RequestParam(defaultValue = "10") Integer size) {
        Page<User> p=new Page<>();
        p.setCurrent(current);
        p.setSize(size);
        Page<User> userPage = userService.page(p);
        Page<UserVO> userVoPage=new Page<>();
        BeanUtils.copyProperties(userPage,userVoPage);

        List<UserVO> userVOList = userPage.getRecords().stream().map(item -> {
            UserVO userVO=new UserVO();
            BeanUtils.copyProperties(item,userVO);
            return userVO;
        }).collect(Collectors.toList());

        userVoPage.setRecords(userVOList);
        return Result.success(userVoPage);
//                return R.data(NoticeWrapper.build().pageVO(pages));
    }

    /**
     * 新增或修改
     */
    @PostMapping("/submit")
    @ApiOperation(value = "新增或修改", notes = "传入userQuery")
    public Result<Object> submit(@Validated(UserQuery.Save.class) @RequestBody UserQuery userQuery) {
        User user=new User();
        BeanUtils.copyProperties(userQuery,user);
        return Result.success(userService.saveOrUpdate(user));
    }

    /**
     * 根据名字更新
     */
    @PostMapping("/updateByName")
    @ApiOperation(value = "根据名字更新用户", notes = "传入userQuery")
    public Result<Object> updateByName(@Validated(UserQuery.UpdateByName.class) @RequestBody UserQuery userQuery) {
        User user=new User();
        BeanUtils.copyProperties(userQuery,user);
        return Result.success(userService.updateByName(user));
    }


}
