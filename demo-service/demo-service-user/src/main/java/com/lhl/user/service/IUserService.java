package com.lhl.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhl.user.model.entity.User;

/**
 *
 * 为了便于排错，在 Service中的CURD方法名要与Mapper中的区别开来：
 *
 * 示例：
 * Service:get 、save 、 update、 remove
 * Mapper:select 、 insert 、update 、 delete
 *
 * @author LinHongli
 */
public interface IUserService extends IService<User> {
    /**
     * 根据名字修改
     * @param user 实体对象
     * @return boolean
     */
    boolean updateByName(User user);
}
